tool
extends EditorPlugin


func _enter_tree():
	# Add FieldOfView2D node
	add_custom_type("FieldOfView2D", "Node2D", preload("FieldOfView2D.gd"), preload("icon.png"))


func _exit_tree():
	# Clean plugin
	remove_custom_type("FieldOfView2D")

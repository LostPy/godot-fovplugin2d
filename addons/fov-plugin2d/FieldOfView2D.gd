extends Node2D


export(float) var field_of_view: float = 60.0
export(float) var distance_of_view: float = 100.0
export(float) var view_detail: float = 60.0  # discretisation paraeter of view

export(bool) var show_fov: bool = false


export(Color) var fov_color: Color = Color("#b23d7f0b")

export(Array) var groups_to_detects: Array = []

var in_view: Array = []

# Buffer to target points
var points_arc: Array = []
var is_updated: bool = false

func _enter_tree() -> void:
	pass

func _exit_tree() -> void:
	in_view.clear()
	groups_to_detects.clear()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:
	is_updated = true
	check_view()
	update()

func _draw() -> void:
	if show_fov && is_updated:
		draw_fov()
		is_updated = false

func draw_fov() -> void:
	for point in points_arc:
		draw_line(get_position(), point.pos, fov_color)


func vec_from_angle(angle: float) -> Vector2:
	return Vector2(cos(angle), sin(angle))

func check_view() -> void:
	var dir_angle = transform.get_rotation()
	var start_angle = dir_angle - deg2rad(field_of_view / 2)
	var end_angle = dir_angle + deg2rad(field_of_view / 2)
	
	points_arc = []
	in_view = []
	
	var space_state = get_world_2d().direct_space_state
	
	for deg_angle in range(rad2deg(start_angle), rad2deg(end_angle), field_of_view / view_detail):
		var current_angle = deg2rad(deg_angle)
		var point = get_position() + vec_from_angle(current_angle) * distance_of_view
		
		var result = space_state.intersect_ray(get_global_transform().origin, to_global(point), [get_parent()])
		
		if result.empty():
			points_arc.append({"pos": point})
			continue
		
		for group in groups_to_detects:
			if result.collider.get_groups().has(group):
				points_arc.append({"pos": to_local(result.position)})
				if not result.collider in in_view:
					in_view.append(result.collider)

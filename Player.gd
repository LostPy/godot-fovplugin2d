extends Area2D

export(float) var speed: float = 100.0
var screen_size
var time_elapsed = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_position(delta)
	time_elapsed += delta
	if time_elapsed > 2.0:
		print("ennemy in view: ", $FieldOfView2D.in_view.size())
		time_elapsed = 0.0

func update_position(delta):
	var velocity = Vector2.ZERO
	if Input.is_action_pressed("move_right"):
		velocity.x += 1
	if Input.is_action_pressed("move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("move_bottom"):
		velocity.y += 1
	velocity = velocity.normalized() * speed
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)

extends Node


export(int) var obstacle_count: int = 10
export(PackedScene) var obstacle_scene


# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(obstacle_count):
		var pos = Vector2.ZERO
		pos.x = rand_range(0.0, 1000)
		pos.y = rand_range(0.0, 700)
		
		var obstacle = obstacle_scene.instance()
		obstacle.position = pos
		add_child(obstacle)

